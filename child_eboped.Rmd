---
output: 
  pdf_document: 
    fig_height: 4
header-includes:
  - \usepackage{booktabs}
  - \usepackage{placeins}
  - \usepackage{float}
  - \usepackage{tabu}
---
\setlength{\unitlength}{1cm}

\begin{picture}(0,0)(-12.5, -1)
\includegraphics[height = 1.1 cm]{logo/LogoCermel.png}
\end{picture}

\begin{center}
\Large{\textbf{Ebolaped EDCTP presentation}}
\end{center}

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE, message = FALSE)
options(knitr.kable.NA = "-")
library(dplyr)
library(knitr)
library(kableExtra)

```
